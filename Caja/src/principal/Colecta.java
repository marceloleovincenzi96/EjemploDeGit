package principal;

public class Colecta {
	
	//prueba
	
	private double montoARecaudar;
	private double montoRecaudado;
	private double maximaDonacion;
	
	public Colecta (double montoObjetivo) {
		
		this.montoARecaudar = montoObjetivo;
		
	}
	
	public double calcularRecaudacion () {
		
		return this.montoRecaudado;
	}
	
	public void donar (double montoADonar) {
		
		if (montoADonar > 0) {
			
			this.montoRecaudado += montoADonar;
		}
		
		if (montoADonar > this.maximaDonacion) {
			
			this.maximaDonacion = montoADonar;
		}
	}
	
	public double calcularDonacionMaxima () {
		
		return this.maximaDonacion;
	}
	
	public double calcularRecaudacionFaltante () {
		
		return this.montoARecaudar - this.montoRecaudado;
	}
	
	
	

}
